package models;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;

import views.WFrame;

public class Fucsia_Frame extends WFrame {
	
	public Fucsia_Frame(Frame parentFrame) {
		super(parentFrame);
		initComponents();
	}
	
	public void initComponents() {
		setSize(500,250);
	}
	public void paint(Graphics g) {
		g.setColor(Color.decode("#FD3F92"));
		g.fillRect(0, 0, 500, 250);
	}
}
