package models;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;

import views.WFrame;

public class Blanco_Frame extends WFrame {
	
	public Blanco_Frame(Frame parentFrame) {
		super(parentFrame);
		initComponents();
	}
	
	public void initComponents() {
		setSize(500,250);
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 500, 250);
	}
}
