package models;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import views.BorderPanel;
import views.WFrame;

public class Calculator extends WFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Label label1, label2, label3;
	private TextField textField1, textField2 ,textField3;
	private CheckboxGroup group1;
	private Checkbox checkbox1, checkbox2, checkbox3, checkbox4;
	private Button button1, button2;
	private BorderPanel panel1;
	
	
	public Calculator(Frame parentFrame) {
		super(parentFrame);
		initComponents();
	}
	
	@Override
	public void initComponents() {
		setLayout(null);
		label1 = new Label("Primer valor");label1.setBounds(20, 50, 100, 32);add(label1);
		textField1 = new TextField("");textField1.setBounds(122, 50, 250, 32);add(textField1);
		
		
		label2 = new Label("Segundo valor");label2.setBounds(20, 84, 100, 32);add(label2);
		textField2 = new TextField("");textField2.setBounds(122, 84, 250, 32);add(textField2);
		
		
		label3 = new Label("Resultado");label3.setBounds(20, 118, 100, 32);add(label3);
		textField3 = new TextField("");textField3.setBounds(122, 118, 250, 32);textField3.setEnabled(false);
		add(textField3);
		
		panel1 = new BorderPanel();
		panel1.setBorderVisible(true);
		panel1.setBounds(375, 47, 113, 120);
		group1 = new CheckboxGroup();
		checkbox1 = new Checkbox("Suma\t\t\t\t  ", group1, true);add(checkbox1);
		checkbox2 = new Checkbox("Resta\t\t\t\t  ", group1, false);add(checkbox2);
		checkbox3 = new Checkbox("Multiplicacion", group1, false);add(checkbox3);
		checkbox4 = new Checkbox("Division\t\t    ", group1, false);add(checkbox4);
		panel1.add(checkbox1);
		panel1.add(checkbox2);
		panel1.add(checkbox3);
		panel1.add(checkbox4);
		add(panel1);
		
		button1 = new Button("Calcular");button1.setBounds(310, 175, 100, 32);add(button1);
		button1.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				
				double variable1, variable2, resultado;
				String s1,s2;
				try {
					s1 = textField1.getText();
					variable1 = Double.parseDouble(s1);
					s2 = textField2.getText();
					variable2 = Double.parseDouble(s2);
					
					if(checkbox1.getState()==true) {
						resultado =variable1 + variable2;
						textField3.setText(String.valueOf(resultado));
					}
					if(checkbox2.getState()==true) {
						resultado =variable1 - variable2;
						textField3.setText(String.valueOf(resultado));
					}
					if(checkbox3.getState()==true) {
						resultado =variable1 * variable2;
						textField3.setText(String.valueOf(resultado));
					}
					if(checkbox4.getState()==true) {
						resultado =variable1 / variable2;
						textField3.setText(String.valueOf(resultado));
					}
					
				}catch(NumberFormatException nfe) {
				
				}
				
			}
		});
		
		button2 = new Button("Borrar");button2.setBounds(100, 175, 100, 32);add(button2);
		button2.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				textField1.setText("");
				textField2.setText("");
				textField3.setText("");
			}
		});
		
		
		setTitle("Calculador");
		setSize(500,250);
	}
}
