package views;

import java.awt.Button;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import misc.event.FormInterface;
import models.Azul_Blanco_Frame;
import models.Azul_Frame;
import models.Blanco_Frame;
import models.Calculator;
import models.Fucsia_Frame;

public class MainFrame extends Frame implements FormInterface{

	private Button button1, button2, button3, button4, button5;
	private Frame instance;
	private BorderPanel panel1;
	
	public MainFrame(){
		initComponents();
		instance = this;
	}

	@Override
	public void initComponents() {
		setLayout(null);
		panel1 = new BorderPanel();
		panel1.setBorderVisible(true);
		panel1.setBounds(panel1.getBorderBounds());
		
		button1 = new Button("Azul");button1.setBounds(160, 90, 100, 32);add(button1);
		button2 = new Button("Azul y Blanco");button2.setBounds(340, 90, 100, 32);add(button2);
		button3 = new Button("Blanco");button3.setBounds(160, 180, 100, 32);add(button3);
		button4 = new Button("Fucsia");button4.setBounds(340, 180, 100, 32);add(button4);
		button5 = new Button("Calculador");button5.setBounds(250, 265, 100, 32);add(button5);
		setSize(600,400);
		this.setResizable(false);
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		button1.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Azul_Frame a = new Azul_Frame(instance);
				a.showForm();
			}
		});
		
		button2.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Azul_Blanco_Frame ab =new Azul_Blanco_Frame(instance);
				ab.showForm();
			}
		});
		
		button3.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Blanco_Frame b = new Blanco_Frame(instance);
				b.showForm();
			}
		});
		
		button4.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Fucsia_Frame f =new Fucsia_Frame(instance);
				f.showForm();
			}
		});
		
		button5.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				Calculator cl = new Calculator(instance);
				cl.showForm();
			}
		});
		
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showForm() {
		setVisible(true);
		setLocationRelativeTo(null);
		toFront();
	}
}
